/**
 * Creates a new instance of Pizza
 * @param {String} kind 
 */

const Pizza = function(kind) {
   this.kind = kind; 
   this.slices = 8; 
};

Pizza.prototype.eatSlice = function() {
   this.slices--;
}

Pizza.prototype.toString = function() {
   return `We ate ${this.slices} slices of ${this.kind} pizza`;
}
const firstPizza = new Pizza('Pepperoni');
const secondPizza = new Pizza('Cheese');

firstPizza.eatSlice();
console.log(firstPizza.toString());
secondPizza.eatSlice();
secondPizza.eatSlice();
secondPizza.eatSlice();
secondPizza.eatSlice();
secondPizza.eatSlice();
console.log(secondPizza.toString());
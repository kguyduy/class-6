$(document).ready(function() {
  // If an li element is clicked, toggle the class "done" on the <li>

  function listToggle() {
    let $this = $(this);
    $this.toggleClass("done");
  }
  $("li").on("click", listToggle);

  // If a delete link is clicked, delete the li element / remove from the DOM

  function clickDelete() {
    $(this).parent().fadeOut(500);
  }
  $("a.delete").click(clickDelete);

  // If a "Move to..."" link is clicked, it should move the item to the correct
  // list.  Should also update the working (i.e. from Move to Later to Move to Today)
  // and should update the class for that link.
  // Should *NOT* change the done class on the <li>
  const $moveToTodayButton = $(".move");
  $moveToTodayButton.on("click", moveToToday);

  function moveToToday() {
    let $this = $(this);
    $(".today-list").append($this.parent());
    $this.parent().toggleClass("done");
    $this.text("Move to Later");
    $this.off("click");
    $this.on("click", moveToLater);
  };

  const $moveToLaterButton = $(".movetoLater");
  $moveToLaterButton.on("click", moveToLater);

  function moveToLater() {
    let $this = $(this);
    $(".later-list").append($this.parent());
    $this.parent().toggleClass("done");
    $this.text("Move to Today");
    $this.off("click");
    $this.on("click", moveToToday);
  };

  // If an 'Add' link is clicked, adds the item as a new list item in correct list
  // addListItem function has been started to help you get going!
  // Make sure to add an event listener to your new <li> (if needed)
  const addListItem = function(e) {
    e.preventDefault();
    let text = $(this).parent().find("input").val();
    //trim the whitespaces to avoid submit empty
    if (text.trim() == "") {
      return;
    }
    let $newListItem = $("<li>");
    let $newSpan = $("<span>");
    $newSpan.text(text);

    //move
    let $moveButton = $("<a>");
    $moveButton.addClass("move");

    //delete
    let $deleteButton = $("<a>");
    $deleteButton.text("Delete");
    $deleteButton.addClass("delete");

    // combine span + a + a
    $newListItem.append($newSpan);
    $newListItem.append($moveButton);
    $newListItem.append($deleteButton);

    // Clears input box after clicking 'Add'
    $("input").val("");
    $newListItem.on('click', listToggle);
    $deleteButton.on('click', clickDelete);
    $moveButton.on('click',  moveToLater);
   

    if (
      $(this).parent().parent().hasClass("today")
    ) {
      $moveButton.text("Move to Later");
      $moveButton.addClass("toLater");
      $(".today-list").append($newListItem);
    } else if (
      $(this).parent().parent().hasClass("later")
    ) {
      $moveButton.text("Move to Today");
      $moveButton.addClass("toToday");
      $(".later-list").append($newListItem);
    }
  };

  // Add this as a listener to the two Add links
  $("a.add-item").click(addListItem);
});

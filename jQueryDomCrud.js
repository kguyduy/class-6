// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
$( document ).ready(function() {

    let $cta = $('a');
    $('a').attr('id', 'cta');
    $cta.text('Buy Now!');
    $('main').append($cta);
  
    // Access (read) the data-color attribute of the <img>,
    // log to the console
    
console.log($('img').attr('data-color'));

    // Update the third <li> item ("Turbocharged"), 
    // set the class name to "highlight"

    $('li').eq(2).addClass('highlight');

//     // Remove (delete) the last paragraph
//     // (starts with "Available for purchase now…")
    $('p').remove();

});

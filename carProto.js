/**
 * Creates a new Car object
 * @constructor
 * @param {String} model 
 */
const Car = function(model) {
    this.model = model; 
    this.currentSpeed = 0; 
};

Car.prototype.accelerate = function() {
    this.currentSpeed++;
}
Car.prototype.brake = function() {
    this.currentSpeed--;
}
Car.prototype.toString = function() {
    return `${this.model} travelling at a speed of ${this.currentSpeed}`;
}
const myCar = new Car('Taurus');

myCar.accelerate();
myCar.accelerate();
console.log(myCar.toString());
myCar.brake();
console.log(myCar.toString());

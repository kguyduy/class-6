// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click


let coordinates = document.getElementsByTagName('td');
for (i = 0; i < coordinates.length; i++){
    coordinates[i].addEventListener('click', function (e) {
        let coordinates = this;
        this.innerHTML = `${e.clientX}, ${e.clientY}`;
        });
        
}
